//
//  CustomLookAndFeel.h
//

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

struct CustomLookAndFeel : LookAndFeel_V4
{
    CustomLookAndFeel()
    {
        Colour lamboElectricGreen = Colour(153, 255, 51);
        
        setColour (Slider::thumbColourId, lamboElectricGreen); // Slider thumb color
        setColour (Slider::textBoxOutlineColourId, Colours::transparentBlack);
        
        setColour (Slider::rotarySliderOutlineColourId, lamboElectricGreen);
        setColour (Slider::rotarySliderFillColourId, lamboElectricGreen);
        
        setColour (ComboBox::backgroundColourId, Colours::transparentBlack);
        setColour (ComboBox::outlineColourId, lamboElectricGreen);
        setColour (ComboBox::arrowColourId, lamboElectricGreen);
        
        setColour (TextButton::buttonColourId, Colours::transparentBlack); // OFF fill color
        setColour (TextButton::buttonOnColourId, Colours::green); // ON fill color
        setColour (TextButton::textColourOffId, Colours::grey); // ON text color
        setColour (TextButton::textColourOnId, Colours::white); // OFF text color
    }

    void drawLinearSlider (
        Graphics& g,
        int   x,
        int   y,
        int   width,
        int   height,
        float sliderPos,
        float minSliderPos,
        float maxSliderPos,
        const Slider::SliderStyle style,
        Slider& slider
    ) override
    {
        if (style == Slider::SliderStyle::LinearBarVertical)
        {
            g.setColour (Colours::grey);
            g.drawRect(0, 0, width, height);
            g.setColour (slider.findColour (Slider::thumbColourId));
            g.drawHorizontalLine(sliderPos, x, width);
        }
        else
        {
            LookAndFeel_V4::drawLinearSlider(g, x, y, width, height, sliderPos, minSliderPos, maxSliderPos, style, slider);
        }
    }
    
    void drawRotarySlider (
        Graphics& g,
        int x,
        int y,
        int width,
        int height,
        float sliderPos,
        const float rotaryStartAngle,
        const float rotaryEndAngle,
        Slider& slider
    ) override
    {
        auto outlineColor = slider.findColour (Slider::rotarySliderOutlineColourId);
        auto fillColor    = slider.findColour (Slider::rotarySliderFillColourId);
        auto offsetX = (width > height) ? (width - height) / 2 : 0;
        auto side = std::min(width, height);
        auto radius = side / 2.f;
        auto bounds = Rectangle<int> (x + offsetX, y, side, side).toFloat();
        auto center = bounds.getCentre();
        auto angle = rotaryStartAngle + sliderPos * (rotaryEndAngle - rotaryStartAngle);

        // derive the mininum slider thumb mark line start and end points based on rotary start angle
        Point<float> minMarkStartPoint = center.getPointOnCircumference(radius - 5, rotaryStartAngle);
        Point<float> minMarkEndPoint = center.getPointOnCircumference(radius + 5, rotaryStartAngle);
        
        // derive the maximum slider thumb mark line start and end points based on rotary end angle
        Point<float> maxMarkStartPoint = center.getPointOnCircumference(radius - 5, rotaryEndAngle);
        Point<float> maxMarkEndPoint = center.getPointOnCircumference(radius + 5, rotaryEndAngle);
        
        g.setOpacity(0.75f);
        g.setColour(outlineColor);

        // draw the rotary knob circle outline
        Path circle;
        circle.addEllipse(bounds);
        g.strokePath(circle, PathStrokeType(1));
        
        // draw the minimum slider thumb mark line
        Path minMarkPath;
        minMarkPath.startNewSubPath(minMarkStartPoint);
        minMarkPath.lineTo(minMarkEndPoint);
        minMarkPath.closeSubPath();
        g.strokePath(minMarkPath, PathStrokeType(1.f));
        
        // draw the maximum slider thumb mark line
        Path maxMarkPath;
        maxMarkPath.startNewSubPath(maxMarkStartPoint);
        maxMarkPath.lineTo(maxMarkEndPoint);
        maxMarkPath.closeSubPath();
        g.strokePath(maxMarkPath, PathStrokeType(1.f));
        
        // draw the knob thumb
        Path thumbPath;
        thumbPath.startNewSubPath(center.getX(), center.getY());
        thumbPath.lineTo( center.getPointOnCircumference(radius, angle) );
        thumbPath.closeSubPath();
        g.setColour (fillColor);
        g.strokePath(thumbPath, PathStrokeType(3.f));

        // draw minimum and maximum value labels
        g.setColour(Colours::white);
        Font font(14);
        g.setFont(font);
        String minMarkLabel = "50%";
        String maxMarkLabel = "200%";

        auto labelHeight = font.getHeight();
        auto minMarkLabelWidth = font.getStringWidth(minMarkLabel);
        auto maxMarkLabelWidth = font.getStringWidth(maxMarkLabel);
        
        Rectangle<float> minMarkRect(minMarkLabelWidth, labelHeight);
        minMarkRect.setCentre( center.getPointOnCircumference(radius + 15, rotaryStartAngle) );
        g.drawFittedText(minMarkLabel, minMarkRect.toNearestInt(), Justification::centred, 1);
                              
        Rectangle<float> maxMarkRect(maxMarkLabelWidth, labelHeight);
        maxMarkRect.setCentre( center.getPointOnCircumference(radius + 15, rotaryEndAngle) );
        g.drawFittedText(maxMarkLabel, maxMarkRect.toNearestInt(), Justification::centred, 1);
    }
};
