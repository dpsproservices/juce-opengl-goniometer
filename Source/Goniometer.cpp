//
//  Goniometer.cpp
//

#include "Goniometer.h"

Goniometer::Goniometer() : offsetX(0.f), scaleValue(1.f)
{
    addAndMakeVisible(goniometerGraph);
}

void Goniometer::update (const AudioBuffer<float>& buffer)
{
    convertSamplesToPoints(buffer);
    goniometerGraph.update(points);
}

void Goniometer::drawLabels (Graphics& g)
{
    auto twoPi_over_8 = MathConstants<float>::twoPi / 8.f;
    auto bounds = getLocalBounds().toFloat().reduced (20.f, 20.f);
    auto width = bounds.getWidth();
    auto height = bounds.getHeight();
    offsetX = (width > height) ? (width - height) / 2 : 0;
    auto offsetY = (height > width) ? (height - width) / 2 : 0;
    auto side = std::min (width, height);
    auto radius = side / 2.f;
    auto circleBounds = Rectangle<int> (bounds.getX() + offsetX, bounds.getY() + offsetY, side, side).toFloat();
    auto center = circleBounds.getCentre();

    g.setColour (Colours::white);
    g.setFont (15);

    Array<String> chars { "+S", "L", "M", "R", "-S" };

    for(int c = 0; c < chars.size(); ++c)
    {
        auto s = chars.getReference(c);
        auto w = Font(15).getStringWidth (s);
        Rectangle<float> r (w, 15);
        r.setCentre ( center.getPointOnCircumference (radius + 12, twoPi_over_8 * (6 + c) ) );
        g.drawFittedText (s, r.toNearestInt(), Justification::centred, 1);
    }
}

// Convert each pair of L/R samples in your buffer into (x,y) points.
void Goniometer::convertSamplesToPoints (const AudioBuffer<float>& buffer)
{
    auto sqrt2inv = 1 / MathConstants<float>::sqrt2;
    auto scale = static_cast<float> (scaleValue.getValue());

    points.clear();

    // add newest Points to the front of the deque
    for (int sampleIndex = 0; sampleIndex < buffer.getNumSamples(); ++sampleIndex)
    {
        auto left = buffer.getSample (0, sampleIndex);
        auto right = buffer.getSample (1, sampleIndex);
        
        // calculate scaled mid side
        auto mid = (left + right) * sqrt2inv * scale;
        auto side = (left - right) * sqrt2inv * scale;

        // normalize y and x coordinate to OpenGL plot boundaries -1..+1
        auto y = jlimit (-1.f, 1.f, mid);
        auto x = jlimit (-1.f, 1.f, side);
        
        auto point = Point<float> { x, y };
        points.push_back (point);
    }
}

void Goniometer::setScaleValue (const float& value)
{
    scaleValue = value;
}

//==============================================================================
// Component overrides
//==============================================================================

void Goniometer::resized()
{
    auto bounds = getLocalBounds().toFloat().reduced(20.f, 20.f);
    auto width = bounds.getWidth();
    auto height = bounds.getHeight();
    auto offsetX = (width > height) ? (width - height) / 2 : 0;
    auto offsetY = (height > width) ? (height - width) / 2 : 0;
    auto side = std::min (width, height);
    auto circleBounds = Rectangle<int> (bounds.getX() + offsetX, bounds.getY() + offsetY, side, side);
    
    // inner circular graph needs to have square bounds same width x height
    goniometerGraph.setBounds (circleBounds);
}

void Goniometer::paint (Graphics& g)
{
    drawLabels(g);
}
