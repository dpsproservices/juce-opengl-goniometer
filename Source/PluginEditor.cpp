/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
OpenGLgoniometerAudioProcessorEditor::OpenGLgoniometerAudioProcessorEditor (OpenGLgoniometerAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p), sliderValue (1.f)
{
    slider.setSliderStyle (Slider::SliderStyle::LinearBarVertical);
    slider.setTextBoxStyle (Slider::NoTextBox, false, 40, 20);
    slider.setLookAndFeel (&customLookAndFeel);
    slider.setRange (0.5f, 2.f);
    slider.getValueObject().referTo (sliderValue);
    slider.onValueChange = [this] ()
    {
        sliderValue = slider.getValue();
        goniometer.setScaleValue ( slider.getValue() );
    };

    addAndMakeVisible (goniometer);
    addAndMakeVisible (slider);
    startTimerHz (60);
    setSize (480, 400);
}

OpenGLgoniometerAudioProcessorEditor::~OpenGLgoniometerAudioProcessorEditor()
{
    stopTimer();
}

//==============================================================================
void OpenGLgoniometerAudioProcessorEditor::paint (juce::Graphics& g)
{
    g.fillAll (Colours::black);
}

void OpenGLgoniometerAudioProcessorEditor::resized()
{
    goniometer.setBounds (30, 30, 340, 340);
    slider.setBounds (400, 50, 40, 300);
}

void OpenGLgoniometerAudioProcessorEditor::timerCallback()
{
    if ( audioProcessor.fifo.pull (buffer) )
    {
        goniometer.update (buffer);
    }
}
