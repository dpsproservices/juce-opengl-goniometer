//
//  PluginEditor.h
//

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "Goniometer.h"
#include "CustomLookAndFeel.h"

//==============================================================================
/**
*/
class OpenGLgoniometerAudioProcessorEditor  : public juce::AudioProcessorEditor, public Timer
{
public:
    OpenGLgoniometerAudioProcessorEditor (OpenGLgoniometerAudioProcessor&);
    ~OpenGLgoniometerAudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

private:
    
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    OpenGLgoniometerAudioProcessor& audioProcessor;
    
    AudioBuffer<float> buffer;
    
    Goniometer goniometer;
    
    CustomLookAndFeel customLookAndFeel;
    
    Slider slider;
    
    Value sliderValue;

    //==============================================================================
    void timerCallback() override;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (OpenGLgoniometerAudioProcessorEditor)
};
