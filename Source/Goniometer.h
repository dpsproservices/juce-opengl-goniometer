//
//  Goniometer.h
//

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <vector>
#include "GoniometerGraph.h"

class Goniometer : public Component
{
public:
    
    Goniometer();
    
    void update (const AudioBuffer<float>& buffer);
    
    void drawLabels (Graphics& g);
    
    // Convert each pair of L/R samples in buffer into (x,y) points.
    void convertSamplesToPoints (const AudioBuffer<float>& buffer);
    
    void setScaleValue (const float& value);
    
    //==============================================================================
    // Component overrides
    //==============================================================================

    void paint (Graphics& g) override;
    
    void resized() override;

    //==============================================================================
    
    float offsetX;

private:

    GoniometerGraph goniometerGraph;

    std::vector<Point<float>> points;

    Value scaleValue; // goniometer scale 50% - 200% range [.5f, 2.f] default 1.f == 100%

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Goniometer)
};
