//
//  Fifo.h
//
//  fifo buffer to transfer AudioBuffer sample data from PluginProcessor to PluginEditor
//

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include <array>
#include <string>

template<typename T>
class Fifo
{
public:
    
    Fifo() { }
    
    void prepare (int samplesPerBlock)
    {
        for ( int i = 0; i < Capacity; ++i )
        {
            buffers[i].setSize(Channels,samplesPerBlock);
            buffers[i].clear();
        }
    }

    bool push (const T& dataToPush)
    {
        auto write = abstractFifo.write(1);
        if ( write.blockSize1 >= 1 )
        {
            buffers[write.startIndex1] = dataToPush;
            return true;
        }
        return false;
    }

    bool pull (T& dataToPull)
    {
        auto read = abstractFifo.read(1);
        if ( read.blockSize1 >= 1 )
        {
            dataToPull = buffers[read.startIndex1];
            return true;
        }
        return false;
    }

private:

    static constexpr int Capacity = 5;
    
    static constexpr int Channels = 2;
    
    AbstractFifo abstractFifo { Capacity };
    
    std::array<T,Capacity> buffers;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Fifo)
};
