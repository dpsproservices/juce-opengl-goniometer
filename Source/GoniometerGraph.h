//
//  GoniometerGraph.h
//
//  Render the goniometer graph and Mid Side plot with OpenGLRenderer
//

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"

#define GoniometerBufferSize 256
#define CircleVertices 1024

class GoniometerGraph : public Component, OpenGLRenderer
{
public:

    GoniometerGraph();
    
    ~GoniometerGraph();
    
    // Updates the goniometer plot points array and triggers render
    void update (const std::vector<Point<float>>& pointsBuffer);
    
    //==============================================================================
    // OpenGLRenderer overrides
    //==============================================================================

    void newOpenGLContextCreated() override;

    void renderOpenGL() override;

    void openGLContextClosing() override;

private:

    OpenGLContext openGLContext;
    
    // Shader program for the background quad texture
    OpenGLShaderProgram backgroundQuadShaderProgram;
    
    // Shader program for the grey circle graph border
    OpenGLShaderProgram circleShaderProgram;
    
    // Shader program for the green goniomter plot
    OpenGLShaderProgram goniometerPlotShaderProgram;
    
    // Shader program for the final screen quad texture
    OpenGLShaderProgram screenQuadShaderProgram;

    // quad vertex array and buffer for full screen texture render
    GLuint quadVertexArrayID;
    GLuint quadVertexBufferID;
    
    // circle XY coordinates for graph border
    GLuint circleVertexArrayID;
    GLuint circleVertexBufferID;

    GLuint backgroundFrameBufferID;
    GLuint screenFrameBufferID;
    
    // OpenGL texture ID for the background + circle border + goniometer plot
    // to be rendered to 1 framebuffer
    GLuint backgroundTextureID;
    
    // OpenGL texture ID to be rendered to the screen
    GLuint screenTextureID;

    // last OpenGL error message
    String statusText;

    // goniometer plot points OpenGL XY coordinates for shader
    GLfloat points [GoniometerBufferSize * 2];
    
    // graph circle border verteces XY positions for shader
    GLfloat circle [CircleVertices * 2];
    
    // quad vertex array for full screen texture render
    GLfloat quadVertices[24] = {
        // positions   // texCoords
        -1.0f,  1.0f,  0.0f, 1.0f,
        -1.0f, -1.0f,  0.0f, 0.0f,
         1.0f, -1.0f,  1.0f, 0.0f,

        -1.0f,  1.0f,  0.0f, 1.0f,
         1.0f, -1.0f,  1.0f, 0.0f,
         1.0f,  1.0f,  1.0f, 1.0f
    };
    
    bool silentInput;
    
    /*
        fragment shader will need following:
        layout(location = 0) out vec3 color;
     
        This means that when writing in the variable “color”,
        we will actually write in the Render Target 0,
        which happens to be our texture because DrawBuffers[0] is GL_COLOR_ATTACHMENTi,
        which is, in our case, renderedTexture.
     
        "color" will be written to the first buffer because of layout(location=0).
        The first buffer is GL_COLOR_ATTACHMENT0 because of DrawBuffers[1] = {GL_COLOR_ATTACHMENT0}
        GL_COLOR_ATTACHMENT0 has renderedTexture attached, so this is where your color is written.
        In other words, you can replace GL_COLOR_ATTACHMENT0 by GL_COLOR_ATTACHMENT2 and it will still work.
        Note : there is no layout(location=i) in OpenGL < 3.3, but you use glFragData[i] = mvvalue anyway.
    */

    // pass through vertex shader first texture quad
    const char* backgroundQuadVertexShaderString = R"(
        #version 330 core
        layout(location = 0) in vec2 vertexPosition;
        layout(location = 1) in vec2 textureCoordinates;
        out vec2 TexCoords;
        void main()
        {
            TexCoords = textureCoordinates;
            gl_Position = vec4(vertexPosition, 0.0, 1.0);
        }
    )";
    
    // pass through fragment shader first texture quad
    const char* backgroundQuadFragmentShaderString = R"(
        #version 330 core
        out vec4 FragColor;
        in vec2 TexCoords;
        uniform sampler2D u_texture;
        void main()
        {
            FragColor = texture(u_texture, TexCoords);
        }
    )";
    
    // border circle vertex shader
    const char* circleVertexShaderString = R"(
        #version 330 core
        layout(location = 0) in vec2 vertexPosition;
        void main()
        {
            gl_Position = vec4(vertexPosition, 0.0, 1.0);
        }
    )";
    
    // border circle fragment shader
    const char* circleFragmentShaderString = R"(
        #version 330 core
        out vec4 FragColor;
        uniform vec4 u_color;
        void main()
        {
            FragColor = u_color;
        }
    )";
    
    // vertex shader draw goniometer plot vertices
    const char* goniometerPlotVertexShaderString = R"(
        #version 330 core
        layout(location = 0) in vec2 vertexPosition;
        layout(location = 1) in vec2 textureCoordinates;
        out vec2 TexCoords;
        void main()
        {
            TexCoords = textureCoordinates;
            gl_Position = vec4(vertexPosition, 0.0, 1.0);
        }
    )";
    
    // takes average color of surrounding sampled pixels  
    const char* goniometerPlotFragmentShaderString = R"(
        #version 330 core
        precision highp float;
        in vec2 TexCoords;
        layout(location = 0) out vec4 FragColor;
        uniform sampler2D u_texture;
        uniform vec4 u_color;
        uniform vec2 u_resolution;
        uniform float u_points[512];
        void main()
        {
            vec4 color = vec4(0.0, 0.0, 0.0, 0.0);
            color += texture(u_texture, (gl_FragCoord.xy + vec2(1, 1)) / u_resolution.xy);
            color += texture(u_texture, (gl_FragCoord.xy + vec2(-1, 1)) / u_resolution.xy);
            color += texture(u_texture, (gl_FragCoord.xy + vec2(1, -1)) / u_resolution.xy);
            color += texture(u_texture, (gl_FragCoord.xy + vec2(-1, -1)) / u_resolution.xy);
            color /= 4.0;
            float x = u_points[0];
            float y = u_points[1];
            vec2 plotUV = (vec2(x, y) + vec2(1, 1)) / 2;
            if ( distance( TexCoords, plotUV ) < 0.005 )
            {
                color = u_color;
            }
            for (int i = 1; i < 512; ++i)
            {
                x = u_points[i];
                y = u_points[i + 1];
                plotUV = (vec2(x, y) + vec2(1, 1)) / 2;
                if ( distance( TexCoords, plotUV ) < 0.005 )
                {
                    color = u_color;
                }
            }
            FragColor = color;
        }
    )";

    // pass through vertex shader final screen render quad
    const char* screenQuadVertexShaderString = R"(
        #version 330 core
        layout(location = 0) in vec2 vertexPosition;
        layout(location = 1) in vec2 textureCoordinates;
        out vec2 TexCoords;
        uniform vec2 u_resolution;
        void main()
        {
            TexCoords = textureCoordinates;
            gl_Position = vec4(vertexPosition, 0.0, 1.0);
        }
    )";
    
    // pass through fragment shader final screen render quad
    const char* screenQuadFragmentShaderString = R"(
        #version 330 core
        out vec4 FragColor;
        in vec2 TexCoords;
        uniform sampler2D u_texture;
        void main()
        {
            FragColor = texture(u_texture, TexCoords);
        }
    )";

    //==============================================================================
    
    // clears the points array to zeros
    void clearPoints();
    
    // compiles the OpenGL shader programs and outputs error status message
    void compileShaderPrograms();
    
    // setup the 1st framebuffer in which the background, circle graph and plot
    // are rendered into a texture which is bound to the framebuffer
    void setupFrameBuffer1(const int& width, const int& height);
    
    // setup the 2nd framebuffer
    void setupFrameBuffer2(const int& width, const int& height);
    
    // generate and bind the vertex array and buffer for the circle
    void setupCircleBuffer();
    
    // calculate and set the circle vertex array
    void setupCircleVertices();
    
    // generate and bind the vertex array and buffer for the quad
    void setupQuadBuffer();
    
    // render the background texture, circle, and plot to the framebuffer screen texture
    void renderToFrameBuffer();
    
    // render the screen texture
    void renderToScreen();

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GoniometerGraph)
};
