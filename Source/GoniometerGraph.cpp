//
//  GoniometerGraph.cpp
//
//  Render the goniometer graph and Mid Side plot with OpenGLRenderer
//

#include "GoniometerGraph.h"

GoniometerGraph::GoniometerGraph() :
    backgroundQuadShaderProgram(openGLContext),
    circleShaderProgram(openGLContext),
    goniometerPlotShaderProgram(openGLContext),
    screenQuadShaderProgram(openGLContext),
    backgroundFrameBufferID(0),
    screenFrameBufferID(0),
    backgroundTextureID(0),
    screenTextureID(0),
    silentInput(true)
{
    openGLContext.setOpenGLVersionRequired (OpenGLContext::OpenGLVersion::openGL3_2);

    // setContinuousRepainting(true) continuously calls render() not paint() nor repaint()
    // so setting to false to manually triggerRepaint() in update()
    openGLContext.setContinuousRepainting(false);
    
    // not using paint() method and rendering in an OpenGLRenderer, so disabled to improve performance.
    // must be called BEFORE attaching OpenGLContext to Component
    openGLContext.setComponentPaintingEnabled(false);

    // Gives the OpenGLContext this OpenGLRenderer to use to do the drawing
    // must be called BEFORE attaching OpenGLContext to Component
    openGLContext.setRenderer(this);
    
    // Attach the OpenGLContext to this Component, the context will be floated above the target component
    openGLContext.attachTo(*this);

    clearPoints();

    DBG("GoniometerGraph");
}

GoniometerGraph::~GoniometerGraph()
{
    // Tell the OpenGLContext to stop using this Component.
    openGLContext.detach();
    
    DBG("~GoniometerGraph");
}

// clears the points array to zeros
void GoniometerGraph::clearPoints()
{
    int j = 0;
    for (int i = 0; i < GoniometerBufferSize; ++i)
    {
        j = i * 2;
        points[j] = (GLfloat)0.f;
        points[j + 1] = (GLfloat)0.f;
    }
}

// Updates the goniometer plot points array and triggers render
void GoniometerGraph::update (const std::vector<Point<float>>& pointsBuffer)
{
    int j = 0;

    for (int i = 0; i < GoniometerBufferSize; ++i)
    {
        auto x = pointsBuffer[i].x;
        auto y = pointsBuffer[i].y;

        j = i * 2;
        points[j] = x;
        points[j + 1] = y;
        
        if (x != 0.f && y != 0.f)
        {
            silentInput = false;
        }
        else
        {
            silentInput = true;
        }
    }

    // triggerRepaint() calls render() but not paint() or repaint()
    // repaint() calls both render() and paint() so it is not used
    openGLContext.triggerRepaint();
}

// compiles the OpenGL shader programs and outputs error status message
void GoniometerGraph::compileShaderPrograms()
{
    // Sets up pipeline of shaders and compiles the program
    
    bool backgroundQuadShaderProgramOK = backgroundQuadShaderProgram.addVertexShader (backgroundQuadVertexShaderString)
    && backgroundQuadShaderProgram.addFragmentShader (backgroundQuadFragmentShaderString)
    && backgroundQuadShaderProgram.link();
    
    bool circleShaderProgramOK = circleShaderProgram.addVertexShader (circleVertexShaderString)
    && circleShaderProgram.addFragmentShader (circleFragmentShaderString)
    && circleShaderProgram.link();
    
    bool goniometerPlotShaderProgramOK = goniometerPlotShaderProgram.addVertexShader (goniometerPlotVertexShaderString)
    && goniometerPlotShaderProgram.addFragmentShader (goniometerPlotFragmentShaderString)
    && goniometerPlotShaderProgram.link();
    
    bool screenQuadShaderProgramOK = screenQuadShaderProgram.addVertexShader (screenQuadVertexShaderString)
    && screenQuadShaderProgram.addFragmentShader (screenQuadFragmentShaderString)
    && screenQuadShaderProgram.link();
    
    if ( backgroundQuadShaderProgramOK && circleShaderProgramOK && goniometerPlotShaderProgramOK && screenQuadShaderProgramOK)
    {
        statusText = "GLSL: v" + String (OpenGLShaderProgram::getLanguageVersion(), 2);
    }
    else if(!backgroundQuadShaderProgramOK)
    {
        statusText = backgroundQuadShaderProgram.getLastError();
    }
    else if(!circleShaderProgramOK)
    {
        statusText = circleShaderProgram.getLastError();
    }
    else if(!goniometerPlotShaderProgramOK)
    {
        statusText = goniometerPlotShaderProgram.getLastError();
    }
    else if(!screenQuadShaderProgramOK)
    {
        statusText = screenQuadShaderProgram.getLastError();
    }

    DBG(statusText);
}

// setup the 1st framebuffer in which the background, circle graph and plot
// are rendered into a texture which is bound to the framebuffer
void GoniometerGraph::setupFrameBuffer1(const int& width, const int& height)
{
    glGenFramebuffers (1, &backgroundFrameBufferID);
    // bind framebuffer
    glBindFramebuffer (GL_FRAMEBUFFER, backgroundFrameBufferID);
    
    // texture we're going to render to
    glGenTextures (1, &screenTextureID);
    // Bind the newly created texture : all future texture functions will modify this texture
    glBindTexture (GL_TEXTURE_2D, screenTextureID);
    
    // Give an empty image to OpenGL ( the last "0" means "empty" )
    // only allocating memory and not actually filling this texture yet.
    // Filling this texture will happen as soon as shader renders to the framebuffer
    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    GLfloat borderColor[4] = { 0, 0, 0, 0 };
    glTexParameterfv (GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
    glBindTexture (GL_TEXTURE_2D, 0);

    // Set "screenTextureID" as our colour attachment #0
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, screenTextureID, 0);
    
    // Set the list of draw buffers.
    GLenum drawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, drawBuffers); // "1" is the size of drawBuffers
    
    // verify framebuffer is ok
    jassert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

// setup the 2nd framebuffer
void GoniometerGraph::setupFrameBuffer2(const int& width, const int& height)
{
    glGenFramebuffers (1, &screenFrameBufferID);
    // bind framebuffer
    glBindFramebuffer (GL_FRAMEBUFFER, screenFrameBufferID);

    // texture we're going to render to
    glGenTextures (1, &backgroundTextureID);
    // Bind the newly created texture : all future texture functions will modify this texture
    glBindTexture (GL_TEXTURE_2D, backgroundTextureID);
    
    // Give an empty image to OpenGL ( the last "0" NULL means "empty" )
    glTexImage2D (GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri (GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    GLfloat borderColor[4] = { 0, 0, 0, 0 };
    glTexParameterfv (GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
    glBindTexture (GL_TEXTURE_2D, 0);

    // Set "backgroundTextureID" as our colour attachment #0
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, backgroundTextureID, 0);

    // Set the list of draw buffers.
    GLenum drawBuffers[1] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, drawBuffers); // "1" is the size of drawBuffers
    
    // verify framebuffer is ok
    jassert(glCheckFramebufferStatus(GL_FRAMEBUFFER) == GL_FRAMEBUFFER_COMPLETE);
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

// generate and bind the vertex array and buffer for the quad
void GoniometerGraph::setupQuadBuffer()
{
    // setup full screen quad vertex buffer
    glGenVertexArrays(1, &quadVertexArrayID);
    glGenBuffers(1, &quadVertexBufferID);
    glBindVertexArray(quadVertexArrayID);
    glBindBuffer(GL_ARRAY_BUFFER, quadVertexBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_DYNAMIC_DRAW);

    // Vertex Viewport Coordinates -1..1
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0,                  // attribute 0. must match the layout 0 in the shader.
        2,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized
        4 * sizeof(float),  // stride
        (void*)0            // array buffer offset
    );
    // Texture coordinates 0..1
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
}

// generate and bind the vertex array and buffer for the circle
void GoniometerGraph::setupCircleBuffer()
{
    // setup full screen quad vertex buffer
    glGenVertexArrays(1, &circleVertexArrayID);
    glGenBuffers(1, &circleVertexBufferID);
    glBindVertexArray(circleVertexArrayID);
    glBindBuffer(GL_ARRAY_BUFFER, circleVertexBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(circle), &circle, GL_DYNAMIC_DRAW);

    // Vertex Viewport Coordinates -1..1
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0,                  // attribute 0. must match the layout 0 in the shader.
        2,                  // size
        GL_FLOAT,           // type
        GL_FALSE,           // normalized
        2 * sizeof(float),  // stride
        (void*)0            // array buffer offset
    );

    // Texture coordinates 0..1
    //glEnableVertexAttribArray(1);
    //glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)(2 * sizeof(float)));
}

// calculate and set the circle vertex array
// repeated rotation algorithm code derived from this site: http://slabode.exofire.net/circle_draw.shtml
void GoniometerGraph::setupCircleVertices()
{
    float theta = MathConstants<float>::twoPi / float(CircleVertices);
    float c = cosf(theta); // precalculate the sine and cosine
    float s = sinf(theta);
    float t = 1.f;
    float x = 1.f; // start at angle zero
    float y = 0.f;
    int p = 0; // circle vertex array index

    for(int i = 0; i < CircleVertices; ++i)
    {
        p = i * 2;
        
        circle[p] = x;
        circle[p + 1] = y;
        
        // apply the rotation matrix
        t = x;
        x = c * x - s * y;
        y = s * t + c * y;
    }
}

// render the background texture, circle, and plot to the framebuffer screen texture
void GoniometerGraph::renderToFrameBuffer()
{
    auto width = getWidth();
    auto height = getHeight();
    
    // ===========================================================================
    // Draw background quad texture
    // ===========================================================================
    
    // Render to our background framebuffer
    glBindFramebuffer(GL_FRAMEBUFFER, backgroundFrameBufferID);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_ALWAYS);

    // Render on the whole framebuffer, complete from the lower left corner to the upper right
    glViewport(0, 0, width, height);

    // Clear the screen
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // draw quad with pass thru vertex and fragment shader
    backgroundQuadShaderProgram.use();

    // Set "u_texture" sampler to use Texture Unit 0
    backgroundQuadShaderProgram.setUniform("u_texture", (GLint)0);

    // Draw the full viewPort sized quad and fill it
    glBindVertexArray(quadVertexArrayID);

    // Bind our texture in Texture Unit 0
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, backgroundTextureID);
    
    // Draw the quad from 2 triangles
    glDrawArrays(GL_TRIANGLES, 0, 6); // 2x3 indices starting at 0 -> 2 triangles

    glBindVertexArray(0);

    // ===========================================================================
    // Draw grey circle graph border
    // ===========================================================================

    circleShaderProgram.use();
    circleShaderProgram.setUniform("u_color", (GLfloat)0.5f, (GLfloat)0.5f, (GLfloat)0.5f, (GLfloat)1.0f);
    
    glBindVertexArray(circleVertexArrayID);

    glDrawArrays (GL_LINE_LOOP, 0, CircleVertices);
    glBindVertexArray(0);

    // ===========================================================================
    // Draw green goniometer plot if there is input data
    // ===========================================================================
    
    if (!silentInput)
    {
        goniometerPlotShaderProgram.use();

        // Set texture sampler to use Texture Unit 0
        goniometerPlotShaderProgram.setUniform("u_texture", (GLint)0);
        goniometerPlotShaderProgram.setUniform("u_color", (GLfloat)0.6f, (GLfloat)1.0f, (GLfloat)0.2f, (GLfloat)1.0f);
        goniometerPlotShaderProgram.setUniform("u_resolution", width, height);
        
        const GLfloat* values = points;
        GLsizei numValues = GoniometerBufferSize * 2;
        goniometerPlotShaderProgram.setUniform("u_points", values, numValues);
        
        glBindVertexArray(quadVertexArrayID);
        
        // Bind our texture in Texture Unit 0
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, backgroundTextureID);

        glDrawArrays(GL_TRIANGLES, 0, 6);
        glBindVertexArray(0);
    }
}

// render the screen texture
void GoniometerGraph::renderToScreen()
{
    auto width = getWidth();
    auto height = getHeight();
    auto renderScale = openGLContext.getRenderingScale(); // x 2
    auto renderedWidth = renderScale * width;
    auto renderedHeight = renderScale * height;

//    DBG("renderToScreen renderScale " + String(renderScale) );

    // unbind framebuffer to render texture to the screen
    //glBindFramebuffer (GL_FRAMEBUFFER, openGLContext.getFrameBufferID());
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_DEPTH_TEST);

    // Render on the whole framebuffer, complete from the lower left corner to the upper right
    glViewport(0, 0, renderedWidth, renderedHeight);

    // Clear the screen
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // use pass through vertex and fragment shader
    screenQuadShaderProgram.use();

    // Set our "u_texture" sampler to use Texture Unit 0
    screenQuadShaderProgram.setUniform("u_texture", (GLint)0);
    screenQuadShaderProgram.setUniform("u_resolution", renderedWidth, renderedHeight);

    // draw the rendered color attachment texture as the texture of the quad to the screen
    glBindVertexArray(quadVertexArrayID);
    //glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, screenTextureID);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

//==============================================================================
// OpenGLRenderer overrides
//==============================================================================

void GoniometerGraph::newOpenGLContextCreated()
{
    auto width = getWidth();
    auto height = getHeight();

    DBG("newOpenGLContextCreated: x " + String(getX()) + " y " + String(getY()) + " w " + String(width) + " h " + String(height));
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    // Clear the screen
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    // calculate and initialize the circle vertices
    setupCircleVertices();

    // setup vertex buffers for the shaders
    setupQuadBuffer();
    setupCircleBuffer();

    // setup the framebuffers and textures
    setupFrameBuffer1(width, height);
    setupFrameBuffer2(width, height);
    
    // Create and compile our GLSL program from the vertex and fragment shaders
    compileShaderPrograms();
}

void GoniometerGraph::renderOpenGL()
{
    // render the sampled texture with "backgroundTextureID" into the 1st frame buffer
    renderToFrameBuffer();

    // render the sampled texture with "screenTextureID" to the screen
    renderToScreen();
}

void GoniometerGraph::openGLContextClosing()
{
    glDeleteBuffers(1, &circleVertexBufferID);
    glDeleteBuffers(1, &quadVertexBufferID);

    glDeleteTextures(1, &backgroundTextureID);
    glDeleteTextures(1, &screenTextureID);
    
    glDeleteFramebuffers(1, &backgroundFrameBufferID);
    glDeleteFramebuffers(1, &screenFrameBufferID);

    glDeleteVertexArrays(1, &circleVertexArrayID);
    glDeleteVertexArrays(1, &quadVertexArrayID);
    
    backgroundQuadShaderProgram.release();
    circleShaderProgram.release();
    goniometerPlotShaderProgram.release();
    screenQuadShaderProgram.release();

    DBG("openGLContextClosing");
}
